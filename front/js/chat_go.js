$( document ).ready(function() {

	var conexion_final;
	var user_name;

	$('#form_registro').on('submit', function(e) {
		user_name = $('#user_name').val()
		if(user_name.trim() !=""){
			e.preventDefault();
			$.ajax({
				type: "post",
				url: "/validate",
				data: {
					'user_name' : user_name
				},
				success: function(data){
					// alert("Ya tenemos respuesta")
					validate_response(data)
				}
			});
		}else{
			alert("Ingrese su nombre de usuario para continuar")
			e.preventDefault();
		}
	});
	
	function validate_response(data){
		obj = JSON.parse(data);
		// alert(obj.valid)
		if (obj.valid === true){
			create_conection();
		}else{
			alert("Acceso invalido, Inténtelo de nuevo!")
			location.reload();
		}
	}

	function create_conection(){
		var conexion = new WebSocket("ws://localhost:8000/ws/" + user_name);
		const usern= document.getElementById('user');
		usern.innerHTML= user_name;   	
		conexion_final = conexion;
		conexion.onopen = function(){
			conexion.onmessage = function(response){
				console.log(response)
				val = $("#chat_area").val();
		   		$("#chat_area").val(val + "\n" + response.data); 
			}
		}
		$("#registro").hide();
   		$("#container_chat").show();
		   
	}

    $('#form_message').on('submit', function(e) {
    	e.preventDefault();
    	conexion_final.send($('#msg').val());
    	$('#msg').val("")
    });
});
